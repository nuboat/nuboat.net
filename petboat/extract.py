#! /usr/bin/python
###################################################
 # petboat.commons.py
 # 2011 nuboat.net. All rights reserved
#

import httplib2
from BeautifulSoup import BeautifulSoup
import re


__author__="Keerati"
__date__ ="$Nov 12, 2011 10:07:45 PM$"



MAIN_URL = "http://iampetdo.com"
LISTING_PAGE_URL = MAIN_URL + "/comics"


def extract_comic_content(url, client = None):
    if client == None: client = httplib2.Http()
    response, content = client.request(MAIN_URL + url)
    html_content = BeautifulSoup(content)
    comic_div = str(html_content.findAll("div",attrs = {"class":"field field-type-filefield field-field-cartoon-image"})[0])
    comic_content = BeautifulSoup(comic_div)
    image_list = comic_content.findAll("img")
    result = []
    for img in image_list:
        result.append(img["src"])
    return result

def get_new_comics_list(latest_id = 0):
    client = httplib2.Http()
    result_list = {}
    end_process = False
    current_page_no = 0
    last_page_no = None
    result, end_process = extract_comics_list(client, current_page_no,latest_id)
    result_list.update(result)
    while not end_process:
        current_page_no += 1
        result, end_process = extract_comics_list(client, current_page_no,latest_id)
        result_list.update(result)
        #print "continue"
        #raw_input()

    #print "return"
    return result_list

# ============== utilities function =======================
def create_comics_dict(**kwargs):
    result = {}
    for k,v in kwargs.items():
        if v != None: result.update({k:v})
    return result

def extract_comics_list(client, page, latest_id):
    url = LISTING_PAGE_URL if page == 0 else LISTING_PAGE_URL + "?page=" + str(page)
    response, content = client.request(url)

    html_content = BeautifulSoup(content)
    maincontent = html_content.findAll("div",id="maincontentpage")
    searchable_maincontent = BeautifulSoup("".join([str(tag) for tag in maincontent]))

    """
    last_page_tag = searchable_maincontent.findAll("a", title="Go to last page")[0]
    last_page_url = last_page_tag["href"]
    last_page_number = int(last_page_url[ last_page_url.rfind("=") + 1 : ])
    """
    #print 'last page no', last_page_number

    comics_list = searchable_maincontent.findAll("a", attrs={"href": re.compile("/comics/[0-9]*") })
    result_list = {}
    id = None
    end_process = False
    for link in comics_list:
        key = link.get("href")
        id = int(key[key.rfind("/") + 1: ])
        if id == latest_id:
            end_process = True
            break

        img = getattr(link,"img")
        if img: img = img.get("src")
        title = link.get("title")
        result = result_list.get(key)
        comics = create_comics_dict(img = img, title=title)
        if result:
            result_list[key].update(comics)
        else:
            result_list.update({key:comics})

    if id == 1: end_process = True

    return result_list, end_process
# ============== end utilities function =======================

if __name__ == "__main__":
    print '======example result for get_new_comics_list===='
    result = get_new_comics_list(407)
    print 'result', result
    print 'detail for the dict'
    for k,v in result.items():
        print 'key',k,'value',v
    print '=================end result===================== '
    print '======example result for extract_comic_content===='
    result = extract_comic_content("/comics/409")
    print 'result', result
    print '=================end result===================== '
