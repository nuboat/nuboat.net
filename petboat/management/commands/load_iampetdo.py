#! /usr/bin/python
###################################################
 # petboat.admin.py
 # 2011 nuboat.net. All rights reserved
#

from datetime import datetime
from django.core.management.base import BaseCommand
from petboat.views import find_latest
from petboat.models import Episode
from petboat.extract import get_new_comics_list, extract_comic_content
from django.core.exceptions import ObjectDoesNotExist


__author__="nuboat"
__date__ ="$Nov 12, 2011 10:07:45 PM$"



class Command(BaseCommand):

    def get_of_create(self, no):
        try:
            epi = Episode.objects.get(no=no)
        except ObjectDoesNotExist:
            epi = Episode()
            epi.no = no
            epi.datetime = datetime.now()

        return epi

    def find_latest_no(self):
        epi = find_latest(None)
        return epi.no

    def handle(self, *args, **options):
        list_episode = get_new_comics_list(self.find_latest_no())
        for k,v in list_episode.items():
            no = k.split("/")[2]
            print str(no) + ":" + str(v)

            urls = extract_comic_content(k)

            epi = self.get_of_create(no)
            epi.name = v['title']
            epi.url1 = urls[0]
            epi.url2 = urls[1]
            epi.url3 = urls[2]
            epi.url4 = urls[3]
            epi.save()

        print '============================================================='
