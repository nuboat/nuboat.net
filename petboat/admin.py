#! /usr/bin/python
###################################################
 # petboat.admin.py
 # 2011 nuboat.net. All rights reserved
#

from petboat.models import Episode
from django.contrib import admin


__author__="nuboat"
__date__ ="$Nov 7, 2011 3:16:45 AM$"



class EpisodeAdmin(admin.ModelAdmin):
    list_display = ('no', 'name', 'storycredit', 'created')
    search_fields = ('no', 'name', 'storycredit')

admin.site.register(Episode, EpisodeAdmin)
