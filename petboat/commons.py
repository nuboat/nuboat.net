#! /usr/bin/python

from django.utils import simplejson
from django.http import HttpResponse
from django.contrib.auth.models import User


__author__= "Peerapat Asoktummarungsri, nuboat@gmail.com"
__date__  = "$Nov 6, 2011 9:02:09 PM$"


def json_return(request, return_string):
    cb = request.REQUEST.get('callback')
    if cb:
        return_string = '%s(%s)' % (cb, return_string)
        content_type = 'text/javascript'
    else:
        content_type = 'application/json'

    return HttpResponse(return_string, content_type=content_type)

def parse_output(request, return_obj):
    return_string = simplejson.dumps(return_obj)
    return json_return(request, return_string)

def lookup_require(request, fields):
    missing_fields = {}
    for field in fields:
        if request.POST.get(field) is None or request.POST[field].strip() == '':
            missing_fields[field] = 'required'

    return missing_fields

def validation(request, require, code, message):
    missing_fields = lookup_require(request, require)
    if len(missing_fields) != 0:
        return_obj = {
            'code'      : code,
            'message'   : message,
            'fields'    : missing_fields, }
        return parse_output(request, return_obj)
    else:
        return None

def validation_token(request):
    try:
        id = request.POST['userid']
        token = 'sha1$'+request.POST['token']
        user = User.objects.get(id=id, password=token)
        if user.is_active:
            return None

        return_obj = {
            'code'      : '31000',
            'message'   : 'Account inactive', }
        return parse_output(request, return_obj)
    except:
        return_obj = {
            'code'      : '32000',
            'message'   : 'Token invalid', }
        return parse_output(request, return_obj)

def api_return(request, return_string):
    cb = request.REQUEST.get('callback')
    if cb:
        return_string = '%s(%s)' % (cb, return_string)
        content_type = 'text/javascript'
    else:
        content_type = 'application/json'

    return HttpResponse(return_string, content_type=content_type)

def template_service(request, validater, finder, parser):

    params = request;

    #Validation data
    list_error = []
    if validater != None:
        list_error = validater(params)

    #Query data.
    if len(list_error) == 0:
        episode = finder(params)
        result = parser(episode)

        return_obj = {'result': result}
    else:
        return_obj = {'error': list_error}

    #Parse data to json format.
    return_string = simplejson.dumps(return_obj)

    #Return to client
    return api_return(request, return_string)
