#! /usr/bin/python
###################################################
 # petboat.models.py
 # 2011 nuboat.net. All rights reserved
#

from django.db import models


__author__= "Peerapat Asoktummarungsri, nuboat@gmail.com"
__date__  = "$Nov 6, 2011 9:02:09 PM$"



class Episode(models.Model):
    no = models.IntegerField(unique=True)
    name = models.CharField(max_length=256)
    storycredit = models.CharField(max_length=64, null=True)

    url1 = models.CharField(max_length=128)
    url2 = models.CharField(max_length=128)
    url3 = models.CharField(max_length=128)
    url4 = models.CharField(max_length=128)

    created = models.DateTimeField(null=True)
