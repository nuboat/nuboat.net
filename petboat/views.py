#! /usr/bin/python
###################################################
 # petboat.views.py
 # 2011 nuboat.net. All rights reserved
#

from petboat.models import Episode
from petboat.commons import template_service
from django.db.models import Max


__author__= "Peerapat Asoktummarungsri, nuboat@gmail.com"
__date__  = "$Nov 6, 2011 9:02:09 PM$"


# Function of get
def find_episode(request):
    try:
        episode_id = request.GET['no']
        episode = Episode.objects.get(no=episode_id)
    except:
        episode = None

    return episode

# Function of list
def find_list(request):
    params = request.GET
    qs_episode = Episode.objects.filter(no__lte=params['start']).order_by('-no')[:params['limit']]
    return qs_episode

def parser_list(qs_episode):
    list_epi = []
    if qs_episode:
        for epi in qs_episode:
            list_epi.append({'no': epi.no,
                            'name': epi.name,
                            'storycredit': epi.storycredit,
                            'url1': epi.url1,
                            })
    return list_epi


# Function of latest
def find_latest(request):
    episode_id = Episode.objects.all().aggregate(Max('no'))['no__max']
    episode = Episode.objects.get(no=episode_id)
    return episode

def parser_episode(episode):
    if episode != None:
        return {'no': episode.no,
                'name': episode.name,
                'storycredit': episode.storycredit,
                'url1': episode.url1,
                'url2': episode.url2,
                'url3': episode.url3,
                'url4': episode.url4,
                }
    else:
        return {}


# REST Services API
def get(request, no):
    request.GET = {'no':no.strip()}
    return template_service(request, None, find_episode, parser_episode)

def list(request, no, limit):
    request.GET = {'start':int(no.strip()), 'limit':int(limit.strip())}
    return template_service(request, None, find_list, parser_list)

def latest(request):
    return template_service(request, None, find_latest, parser_episode)
