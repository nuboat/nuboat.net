#! /usr/bin/python
###################################################
 # db_route.py
 # 2011 nuboat.net. All rights reserved
#


__author__="nuboat"
__date__ ="$Nov 16, 2011 3:16:45 AM$"



class DefaultRouter(object):
    """ A router to control all database operations on models in
        the analytic application
    """

    def db_for_read(self, model, **hints):
        # "Point all operations on analytic models to 'analytic'"
        if model._meta.app_label == 'connect':
            return 'connect'
        return None

    def db_for_write(self, model, **hints):
        # "Point all operations on analytic models to 'analytic'"
        if model._meta.app_label == 'connect':
            return 'connect'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        # "Allow any relation if a model in analytic is involved"
        if obj1._meta.app_label == 'connect' or obj2._meta.app_label == 'connect':
            return True
        return None

    def allow_syncdb(self, db, model):
        # "Make sure the analytic app only appears on the 'analytic' db"
        if db == 'connect':
            return model._meta.app_label == 'connect'
        elif model._meta.app_label == 'connect':
            return False
        return None
