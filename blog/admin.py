#! /usr/bin/python
###################################################
 # blog.admin.py
 # nuboat.net &copy; 2012 All Rights Reserved
#
from blog.models import *
from django import forms
from django.contrib import admin



__author__= "Peerapat Asoktummarungsri [nuboat@gmail.com]"
__date__  = "Aughust 07, 2012"


class BlogAdminForm(forms.ModelForm):
    class Meta:
        model = Blog

    # Step 2: Override the constructor to manually set the form
    def __init__(self, *args, **kwargs):
        super(BlogAdminForm, self).__init__(*args, **kwargs)

    # Step 3: Override the save method to manually save the model
    def save(self, commit=True):
        model = super(BlogAdminForm, self).save(commit=False)
        model.save()

        for t in Tag.objects.filter(blog__id=model.id):
            t.delete()

        for t in model.tags.split():
            tag = Tag()
            tag.tag = t
            tag.blog = Blog(id=model.id)
            tag.save()

        return model

class BlogAdmin(admin.ModelAdmin):
    form = BlogAdminForm

admin.site.register(Page)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Tag)
