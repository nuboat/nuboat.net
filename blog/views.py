#! /usr/bin/python
###################################################
 # blog.views.py
 # nuboat.net &copy; 2012 All Rights Reserved
#
from blog.models import *
from blog.common import *
from django.template import RequestContext
from django.shortcuts import render_to_response


__author__= "Peerapat Asoktummarungsri [nuboat@gmail.com]"
__date__  = "August 07, 2012"


def home(request):
    list_blog = Blog.latest()
    context = RequestContext(request)
    context.update({'page': 'home'})
    context.update({'list_blog': list(list_blog)})
    return render_to_response('home.htm', context)

def tag(request, tag):
    list_blog = Blog.latest(tag=tag)
    context = RequestContext(request)
    context.update({'page': 'home'})
    context.update({'list_blog': list(list_blog)})
    return render_to_response('home.htm', context)

def blog(request, path):
    try:
        blog = Blog.objects.get(path=path, ispublish='T')
        blog.count()
    except Blog.DoesNotExist:
        return None

    context = RequestContext(request)
    context.update({'page': 'blog'})
    context.update({'blog': blog})
    return render_to_response('blog.htm', context)

def blogbyid(request, blogid):
    try:
        blog = Blog.objects.get(id=int(blogid), ispublish='T')
        blog.count()
    except Blog.DoesNotExist:
        return None

    context = RequestContext(request)
    context.update({'page': 'blog'})
    context.update({'blog': blog})
    return render_to_response('blog.htm', context)

def about(request):
    context = RequestContext(request)
    context.update({'page': 'about'})
    return render_to_response('about.htm', context)
