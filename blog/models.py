#! /usr/bin/python
###################################################
 # blog.models.py
 # nuboat.net &copy; 2012 All Rights Reserved
#
from django.db import models
#from django.db.models import Q
#from django.conf import settings
#from django.contrib.auth.models import User
#from django.utils.translation import ugettext_lazy as _



PUBLISH_CHOICES = (
    (u'T', u'True'),
    (u'F', u'False'),
)

class Page(models.Model):
    name = models.CharField(max_length=256)
    path = models.CharField(max_length=256, unique=True)
    content = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

class Blog(models.Model):
    topic = models.CharField(max_length=256)
    topicphoto = models.ImageField(upload_to='topic', max_length=256, blank=True, null=True)
    description = models.CharField(max_length=512)
    path = models.CharField(max_length=256, unique=True)
    ispublish = models.CharField(max_length=1, choices=PUBLISH_CHOICES)
    content = models.TextField()
    views = models.IntegerField()
    tags = models.CharField(max_length=512, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.topic + ": " + self.description

    def count(self):
        from django.db import connection, transaction
        cursor = connection.cursor()
        cursor.execute("update blog_blog set views = (select views+1 from blog_blog where id = %s) where id = %s", [self.id, self.id])
        transaction.commit_unless_managed()

    @staticmethod
    def latest(tag=None, page=0, max=12):
        qs_blog = Blog.objects.filter(ispublish='T')
        if tag != None:
            qs_blog = qs_blog.filter(id__in=set([ t.blog.id for t in Tag.objects.filter(tag__iexact=tag)]))
        return qs_blog.order_by('-created')[:max]

class Tag(models.Model):
    tag = models.CharField(max_length=256)
    blog = models.ForeignKey(Blog)

    def __unicode__(self):
        return self.tag
