#! /usr/bin/python
###################################################
 # petboat.admin.py
 # 2011 nuboat.net. All rights reserved
#

from taxi.models import *
from django.contrib import admin

class TaxiLogAdmin(admin.ModelAdmin):
    list_display = ('user', 'platenumber', 'comment')
    search_fields = ('user', 'platenumber')

admin.site.register(TaxiLog, TaxiLogAdmin)
