from django.db import models
from django.contrib.auth.models import User


class TaxiLog(models.Model):
    user        = models.ForeignKey(User)
    platenumber = models.CharField(max_length=16)
    comment     = models.CharField(max_length=140)
    lat         = models.CharField(max_length=16, blank=True, null=True)
    lng         = models.CharField(max_length=16, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.created) + ': ' + self.platenumber
