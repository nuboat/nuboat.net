#!/usr/lib/python
from taxi.models import TaxiLog
from django import forms
from django.db import transaction
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from petboat.commons import *
from django.contrib.csrf.middleware import csrf_exempt


f = forms.EmailField()

def find_byuser(request):
    try:
        userid = request.POST['userid']
        qs_taxilog = TaxiLog.objects.filter(user__id=userid)
    except:
        qs_taxilog = None
    return qs_taxilog[:24]

def find_byplate(request):
    try:
        plate = request.POST['plate']
        qs_taxilog = TaxiLog.objects.filter(platenumber=plate)
    except:
        qs_taxilog = None
    return qs_taxilog[:24]

def parser_log(qs_taxilog):
    list_log = []
    if qs_taxilog:
        for log in qs_taxilog:
            list_log.append({'plate'    : log.platenumber,
                             'comment'  : log.comment,
                             'lat'      : log.lat,
                             'lat'      : log.lng, })
    return list_log

@csrf_exempt
def findbyuser(request):
    require = ['userid', 'token']
    result = validation(request, require, '10000', 'Required Field')
    if result is not None:
        return result

    result = validation_token(request)
    if result is not None:
        return result

    return template_service(request, None, find_byuser , parser_log)

@csrf_exempt
def findbyplate(request):
    require = ['userid', 'token', 'plate']
    result = validation(request, require, '10000', 'Required Field')
    if result is not None:
        return result

    result = validation_token(request)
    if result is not None:
        return result

    return template_service(request, None, find_byplate, parser_log)

@csrf_exempt
def authen(request):
    require = ['email', 'password']
    result = validation(request, require, '20000', 'Required Field')
    if result is not None:
        return result

    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(username=email, password=password)

    if user is None:
        return_obj = {
            'code'      : '21000',
            'message'   : 'Authentication Failed', }
        return parse_output(request, return_obj)

    if user.is_active:
        return_obj = {
            'code'      : '00000',
            'message'   : 'Success',
            'id' : user.id,
            'token' : user.password[5:], }
        return parse_output(request, return_obj)
    else:
        return_obj = {
            'code'      : '22000',
            'message'   : 'User is disabled', }
        return parse_output(request, return_obj)

@transaction.commit_on_success
@csrf_exempt
def comment(request):
    require = ['plate', 'userid', 'comment', 'lat', 'lng']
    result = validation(request, require, '10000', 'Required Field')
    if result is not None:
        return result

    result = validation_token(request)
    if result is not None:
        return result

    taxilog = TaxiLog()
    taxilog.user = User(id=request.POST['userid'])
    taxilog.platenumber = request.POST['plate']
    taxilog.comment = request.POST['comment']
    taxilog.lat = request.POST['lat']
    taxilog.lng = request.POST['lng']
    taxilog.save()

    return_obj = {
        'code'      : '00000',
        'message'   : 'Success', }
    return parse_output(request, return_obj)

@transaction.commit_on_success
@csrf_exempt
def register(request):
    require = ['email', 'password']
    result = validation(request, require, '10000', 'Required Field')
    if result is not None:
        return result

    email = request.POST['email']
    password = request.POST['password']

    try :
        f.clean(email)
    except:
        return_obj = {
            'code'      : '12000',
            'message'   : 'Email invalid format.',}
        return parse_output(request, return_obj)

    try:
        user = User.objects.get(username=email)
    except:
        user = None
    if user is not None:
        return_obj = {
            'code'      : '11000',
            'message'   : 'Email Exist',}
        return parse_output(request, return_obj)

    user = User.objects.create_user(email, email, password)
    user.is_staff = False
    user.save()

    return_obj = {
        'code'      : '00000',
        'message'   : 'Success',
        'id'        : user.id,
        'token'     : user.password[5:], }
    return parse_output(request, return_obj)
