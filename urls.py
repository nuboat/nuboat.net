from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

__author__= "Peerapat Asoktummarungsri, nuboat@gmail.com"
__date__  = "Aug 03, 2012"

admin.autodiscover()

urlpatterns = patterns('',
    # nuboat.net
    url(r'^$'   , 'blog.views.home'     , name='p_home'),
    url(r'^me$' , 'blog.views.about'    , name='p_about'),
    url(r'^tag/(?P<tag>[\-\w]+)$'       ,'blog.views.tag'       , name='p_tag'),
    url(r'^blog/(?P<path>[\-\w]+)$'     ,'blog.views.blog'      , name='p_blog'),
    url(r'^blogid/(?P<blogid>[\-\w]+)$' ,'blog.views.blogbyid'  , name='p_blogbyid'),

    (r'^taxi/register$',    'taxi.views.register'),
    (r'^taxi/comment$',     'taxi.views.comment'),
    (r'^taxi/authen$',      'taxi.views.authen'),
    (r'^taxi/findbyuser$',  'taxi.views.findbyuser'),
    (r'^taxi/findbyplate$', 'taxi.views.findbyplate'),


    # petboat
    (r'^petboat/latest$', 'petboat.views.latest'),
    (r'^petboat/get/(?P<no>\d+)$', 'petboat.views.get'),
    (r'^petboat/list/(?P<no>\d+)/(?P<limit>\d+)$', 'petboat.views.list'),

    url(r'^__nuboatmin/', include(admin.site.urls)),
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	(r'^media/(?P<path>.*)$', 'django.views.static.serve',
				{'document_root': settings.MEDIA_ROOT}),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
